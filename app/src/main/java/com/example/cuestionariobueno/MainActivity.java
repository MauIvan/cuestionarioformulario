package com.example.cuestionariobueno;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private RadioButton check1, check2, check3, check4, check5, check6, check7, check8,check9,check10, check11, check12, check13, check14, check15, check16,check17, check18,check19,check20,check21,check22,check23,check24,check25,check26,check27,check28,check29,check30,check31,check32,check33,check34,check35,check36,check37,check38,check39,check40;
    private TextView Imprimir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        check1 = findViewById(R.id.Check1);
        check3 = findViewById(R.id.Check3);
        check6 = findViewById(R.id.Check6);
        check8 = findViewById(R.id.Check8);
        check9 = findViewById(R.id.Check9);
        check15 = findViewById(R.id.Check15);
        check18 = findViewById(R.id.Check18);
        check19 = findViewById(R.id.Check19);
        check21 = findViewById(R.id.Check21);
        check23 = findViewById(R.id.Check23);
        check25 = findViewById(R.id.Check25);
        check27 = findViewById(R.id.Check27);
        check29 = findViewById(R.id.Check29);
        check31 = findViewById(R.id.Check31);
        check34 = findViewById(R.id.Check34);
        check35 = findViewById(R.id.Check35);
        check37 = findViewById(R.id.Check37);
        check39 = findViewById(R.id.Check39);
        Imprimir = findViewById(R.id.Impresion);


    }

    public void Enviar(View view) {
        int check = 0;
        check += check1.isChecked() ? 1 : 0;
        check += check3.isChecked() ? 1 : 0;
        check += check6.isChecked() ? 1 : 0;
        check += check8.isChecked() ? 1 : 0;
        check += check9.isChecked() ? 1 : 0;
        check += check12.isChecked() ? 1 : 0;
        check += check14.isChecked() ? 1 : 0;
        check += check15.isChecked() ? 1 : 0;
        check += check18.isChecked() ? 1 : 0;
        check += check19.isChecked() ? 1 : 0;
        check += check21.isChecked() ? 1 : 0;
        check += check23.isChecked() ? 1 : 0;
        check += check25.isChecked() ? 1 : 0;
        check += check27.isChecked() ? 1 : 0;
        check += check29.isChecked() ? 1 : 0;
        check += check31.isChecked() ? 1 : 0;
        check += check34.isChecked() ? 1 : 0;
        check += check35.isChecked() ? 1 : 0;
        check += check37.isChecked() ? 1 : 0;
        check += check39.isChecked() ? 1 : 0;
        check += check39.isChecked() ? 1 : 0;


        int correctas = 0;
        String Resultado;
        correctas = correctas + check;

        Resultado = String.valueOf(correctas);
        Imprimir.setText("Tuviste " + Resultado + " Preguntas Correctas");

    }
}